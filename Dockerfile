FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
COPY . ./src
WORKDIR /src
RUN dotnet restore
RUN dotnet publish -c Release

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine AS runtime
COPY --from=build /src/KubeWriteFile/bin/Release/netcoreapp2.2/publish app
WORKDIR app
ENTRYPOINT ["dotnet", "KubeWriteFile.dll"]
