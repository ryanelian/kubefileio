﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KubeWriteFile.API
{
    [Route("api/read-file")]
    [ApiController]
    [Produces("application/json")]
    public class ReadFileApiController : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<string>> GetAsync()
        {
            var text = await System.IO.File.ReadAllTextAsync("/blob/hello.txt", System.Text.Encoding.UTF8);
            return text;
        }
    }
}