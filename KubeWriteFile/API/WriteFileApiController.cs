﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KubeWriteFile.API
{
    [Route("api/write-file")]
    [ApiController]
    [Produces("application/json")]
    public class WriteFileApiController : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<bool>> GetAsync()
        {
            if (Directory.Exists("/blob") == false)
            {
                Directory.CreateDirectory("/blob");
            }
            await System.IO.File.WriteAllTextAsync("/blob/hello.txt", "Hello World!", System.Text.Encoding.UTF8);
            return true;
        }
    }
}